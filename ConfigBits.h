/*! \file  ConfigBits.h
 *
 *  \brief Configuration bits for PIC32MX250F128B
 *
 *  Configuration bits are set for a full speed setting.
 *
 *  \li 20 MHz crystal * 20 / 2
 *  \li Watchdog timer disabled
 *  \li Clock switching enabled
 *  \li Peripheral clock divide by 1
 *  \li Secondary oscillator disabled
 *  \li CLK0 signal enabled
 *  \li Primary oscillator mode HS
 *  \li Primary oscillator in use
 *  \li Code protect off
 *  \li Boot flash write protect off
 *
 *  \author jjmcd
 *  \date December 14, 2013, 9:18 AM
 */
/* Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#ifndef CONFIGBITS_SLOW_H
#define	CONFIGBITS_SLOW_H

#ifdef	__cplusplus
extern "C" {
#endif

/*! 20MHz crystal / 4 = 5 MHz */
#pragma config FPLLIDIV =       DIV_4
/*! 5 MHz * 20 = 100 MHz */
#pragma config FPLLMUL  =       MUL_20
/*! 10 MHz / 2 = 50 MHz Fosc */
#pragma config FPLLODIV =       DIV_2
/*! Watchdog timer disabled */
#pragma config FWDTEN   =       OFF
/*! Clock switching enabled, clock monitor enabled */
#pragma config FCKSM    =       CSECME
/*! Peripheral bus clock divisor 50/1 = 50 MHz */
#pragma config FPBDIV   =       DIV_1
/*! CLKO signal enabled */
#pragma config OSCIOFNC =       ON
/*! Primary oscillator mode */
#pragma config POSCMOD  =       HS
/*! Secondary oscillator disabled */
#pragma config FSOSCEN  =       OFF
/*! Oscillator selection, primary oscillator with PLL */
#pragma config FNOSC    =       PRIPLL
/*! Code protect off */
#pragma config CP       =       OFF
/*! Boot flash write protect off */
#pragma config BWP      =       OFF
/*! Program flash write protect off */
#pragma config PWP      =       OFF
/*! Use PGEC1/PGED1 */
#pragma config ICESEL   =       ICS_PGx1
/*! Disable JTAG */
#pragma config JTAGEN   =       OFF
/*! Allow multiple peripheral configurations */
#pragma config PMDL1WAY =       ON
#pragma config IOL1WAY  =       ON

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIGBITS_SLOW_H */
