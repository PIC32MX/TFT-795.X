/*! \file  Delays.h
 *
 *  \brief Manifest constants for timing delays
 *
 *  \author jjmcd
 *  \date December 14, 2013, 9:31 AM
 */
/*
 * Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#ifndef DELAYS_H
#define	DELAYS_H

#ifdef	__cplusplus
extern "C" {
#endif

/*! Delay between cycles, 400K fast but followable */
//#define DELAY 400000
#define DELAY 800000
/*! Bit time delay */
/*  One bit delay is used after setting chip select, and 34 bit
 *  delays after the data is sent.  This leads to longer than
 *  necessary delays. */
#define BITDELAY 1

#ifdef	__cplusplus
}
#endif

#endif	/* DELAYS_H */
