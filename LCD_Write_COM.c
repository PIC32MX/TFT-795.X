/*! \file  LCD_Write_COM.c
 *
 *  \brief Write a command to the TFT
 *
 *
 *  \author jjmcd
 *  \date January 2, 2014, 8:33 AM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include "class_attributes.h"


/*! LCD_Write_COM - */

/*!
 *
 */
void LCD_Write_COM(uint8_t VL)
{
//    cbi(P_RS, B_RS);
    clearRS();
    LCD_Writ_Bus(0x00,VL);

}

//void LCD_Write_COM_DATA(uint8_t VH, uint8_t VL)
void LCD_Write_COM_DATA(uint8_t com1, uint16_t dat1)
{
//    cbi(P_RS, B_RS);
    clearRS();
//    LCD_Writ_Bus(VH,VL);
    LCD_Write_COM(com1);
    LCD_Write_DATA(dat1>>8,dat1&0xff);

}
