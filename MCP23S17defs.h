/*! \file  MCP23S17defs.h
 *
 *  \brief Definitions for MCP23S17
 *
 *  With IOCON.BANK=0, the PORT A and PORT B registers alternate
 *  addresses so the low bit of the address determines the bank
 *  instad of bit 4 as in IOCON.BANK=1.  Thus, IODIRA is at 0x00
 *  and IODIRB is at 0x01.  According to the "REGISTER 1-6" chart
 *  in the datasheet, this is the default setting.
 *
 *  \author jjmcd
 *  \date December 14, 2013, 10:03 AM
 */
/*
 * Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#ifndef MCP23S17DEFS_H
#define	MCP23S17DEFS_H

#ifdef	__cplusplus
extern "C" {
#endif

/*! MCP23S17 #1 chip address */
#define S17_1_ADDR 0x4c
/*! MCP23S17 #2 chip address */
#define S17_2_ADDR 0x48

/*! MCP23S17 IO Direction A Address */
#define S17_IODIRA 0x00
/*! MCP23S17 IO Direction B Address */
#define S17_IODIRB 0x01
/*! MCP23S17 Output Latch A Address */
#define S17_OLATA 0x14
/*! MCP23S17 Output Latch B Address */
#define S17_OLATB 0x15
/*! MCP23S17 Configuration Register A */
#define S17_CONFA 0x0a
/*! MCP23S17 GPIO Pull-up register A */
#define S17_GPPUA 0x06
/*! MCP23S17 GPIO register A */
#define S17_GPIOA 0x12

#define IOCON_BANK   0x80
#define IOCON_MIRROR 0x40
#define IOCON_SEQOP  0x20
#define IOCON_DISSLW 0x10
#define IOCON_HAEN   0x08
#define IOCON_ODR    0x04
#define IOCON_INTPOL 0x02



#ifdef	__cplusplus
}
#endif

#endif	/* MCP23S17DEFS_H */

