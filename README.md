## Graphics TFT Library

This project provides a graphics library for the SainSmart 20-011-918
TFT.  

This version is for the PIC32MX795F512L on the ChipKIT Max32 board
using a purpose build shield described in the `PIC32MZ/TFT-TQFP.git`
repository.
