/*! \file  TFTGannotate.c
 *
 *  \brief Axis annotation routines for TFTG library
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 10:20 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../include/TFT.h"
#include "TFTG.h"
#include "TFTGinternal.h"

/*! calcPower -  Given a number, calculate the power of ten */
int calcPower( int x )
{
  // log10() in XC32 library appears broken, use brute force
  if ( x < 10 )
    return 0;
  if ( x<100 )
    return 1;
  if ( x<1000 )
    return 2;
  if ( x<10000 )
    return 3;
  return 4;
}

/*! iPow - Integer x raised to the y */
int iPow( int x, int y )
{
  double fPow;

  fPow = ceil(pow( (double)x, (double)y ));
  return (int) fPow;
}

/*! TFTGannotateY - Annotate the Y axis */
void TFTGannotateY( void )
{
  int nRange;
  int nPower;
  int nScalePower;
  int nSteps;
  int nStepSize;
  int nThisVal;
  int nThisLoc;
  int i;
  int nTickedRange;
  int nPelsPerTic;

  nRange = nYmax - nYmin;
  nPower = calcPower( nRange );
  nScalePower = nPower-1;
  nStepSize = ( iPow(10,nScalePower) );
  nSteps = nRange / nStepSize;
  if ( nSteps<2 )
    {
      nStepSize = ( iPow(10,nScalePower) )/5;
      nSteps = nRange / nStepSize;
    }
  if ( nSteps>10 )
    {
      nStepSize = 2 * ( iPow(10,nScalePower) );
      nSteps = nRange / nStepSize;
      if ( nSteps>10 )
        {
          nStepSize = 5 * ( iPow(10,nScalePower) );
          nSteps = nRange / nStepSize;
        }
    }

  nTickedRange = nSteps*nStepSize;
  nPelsPerTic = (int)((TFTGYBOTTOM-TFTGYTOP)*(long)nTickedRange/(long)nRange)/nSteps;

  for ( i=0; i<nSteps+1; i++ )
    {
      nThisVal = nYmin + i*nStepSize;
      nThisLoc = TFTGYBOTTOM-i*nPelsPerTic;
      TFTline((TFTGXLEFT-3),nThisLoc,(TFTGXLEFT),nThisLoc);
      TFTprintInt(nThisVal, 2, (TFTGXLEFT-2), (nThisLoc-6));
    }
  TFTline(TFTGXLEFT,TFTGYTOP,TFTGXLEFT,TFTGYBOTTOM);
}

/*! TFTGannotateX - Annotate the X axis */
void TFTGannotateX( void )
{
  int nRange;
  int nPower;
  int nScalePower;
  int nSteps;
  int nStepSize;
  int nThisVal;
  int nThisLoc;
  int i;
  int nTickedRange;
  int nPelsPerTic;
  int nOffset;

  nRange = nXmax - nXmin;
  nPower = calcPower( nRange );
  nScalePower = nPower-1;
  nStepSize = ( iPow(10,nScalePower) );
  nSteps = nRange / nStepSize;
  if ( nSteps<2 )
    {
      nStepSize = ( iPow(10,nScalePower) )/2;
      nSteps = nRange / nStepSize;
    }
  if ( nSteps>10 )
    {
      nStepSize = 2 * ( iPow(10,nScalePower) );
      nSteps = nRange / nStepSize;
      if ( nSteps>10 )
        {
          nStepSize = 5 * ( iPow(10,nScalePower) );
          nSteps = nRange / nStepSize;
        }
    }

  nTickedRange = nSteps*nStepSize;
  nPelsPerTic = (int)((TFTGXRIGHT-TFTGXLEFT)*(long)nTickedRange/(long)nRange)/nSteps;

  for ( i=0; i<nSteps; i++ )
    {
      nThisVal = nYmin + i*nStepSize;
      nThisLoc = TFTGXLEFT+i*nPelsPerTic;
      TFTline(nThisLoc,(TFTGYBOTTOM+1),nThisLoc,(TFTGYBOTTOM+5));
      nOffset = (int)( 4.0 * calcPower(nThisVal) ) + 4;
      TFTprintInt(nThisVal, 0, (nThisLoc-nOffset),(TFTGYBOTTOM+4));
    }
  TFTline(TFTGXLEFT,(TFTGYBOTTOM+1),TFTGXRIGHT,(TFTGYBOTTOM+1));
}


