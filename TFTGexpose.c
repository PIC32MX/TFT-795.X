/*! \file  TFTGexpose.c
 *
 *  \brief Draw the graph frame
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:52 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "../include/TFT.h"
#include "TFTG.h"
#include "TFTGinternal.h"


/*! TFTGexpose - Draw the external, fixed parts of the graph */

/*!
 *
 */
void TFTGexpose( void )
{
  int i;
  int pos;
  int space;

  TFTsetBackColorX(uScreenColor);
  TFTclear();

  if (szTitle[0])
    {
      TFTsetFont((fontdatatype*)BigFont);
      TFTsetColorX(uTitleColor);
      if ( strlen(szTitle)>15 )
        pos = 160 - (13*strlen(szTitle))/2;
      else
        pos = 180 - (13*strlen(szTitle))/2;
      space = 13;
      if(strlen(szTitle)>18)
        {
          space = 12;
          pos = 0;
        }
      TFTprint(szTitle, pos, 2, 0);
    }
  TFTsetFont((fontdatatype*)SmallFont);

  if (szXLabel[0])
    {
      TFTsetColorX(uXLabelColor);
      pos = 180 - (7 * strlen(szXLabel)) / 2;
      TFTprint(szXLabel,pos,227,0);
    }

  if (szYLabel[0])
    {
      TFTsetColorX(uYLabelColor);
      pos = 120 - 5 * strlen(szYLabel);
      for (i = 0; i < strlen(szYLabel); i++)
        {
          TFTprintChar(szYLabel[i], 1, (pos + 10 * i) );
        }
    }

  TFTsetColorX(uFieldColor);
  TFTfillRect(TFTGXLEFT+1, TFTGYTOP, TFTGXRIGHT, TFTGYBOTTOM);

  TFTsetColorX(uAnnotColor);
  if ( nXmax>nXmin )
    TFTGannotateX();
  if ( nYmax>nYmin )
    TFTGannotateY();
/*
  for (i = 0; i < 4; i++)
    {
      printInt((3000 - 1000 * i), TFTPRINTINTRIGHT, 39, (20 + 62 * i));
    }
*/

}
