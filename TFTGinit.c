/*! \file  TFTGinit.c
 *
 *  \brief Initialize the graph
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:48 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
/*! The graphing internal variables are implemented in this module */
/*! The EXTERN macro is defined as extern in TFTGinternal.h if not
 *  previously defined.  Any module including TFTGinternal.h will then
 *  have the variables defined therein as extern.  However, should EXTERN
 *  be defined before including TFTGinternal.h the variables will
 *  actually be implemented.
 */
#define EXTERN
#include "TFTGinternal.h"


/*! TFTGinit - Initialize the graph */

/*!
 *
 */
void TFTGinit( void )
{
  uScreenColor = 0x2104;
  uFieldColor  = 0x0000;
  uAnnotColor  = 0x0451;
  uXLabelColor = 0x2595;
  uYLabelColor = 0x2595;
  uTitleColor  = 0xfea0;
  uLineColor   = 0xcc27;
  nXmin = 0;
//  nXmax = 279;
  nXmax = 0;
  nYmin = 0;
//  nYmax = 190;
  nYmax = 0;
  memset(szTitle,0,sizeof(szTitle));
  memset(szXLabel,0,sizeof(szXLabel));
  memset(szYLabel,0,sizeof(szYLabel));
  nLastX = 0;
  nEraseLast = 1;
  nUseCursor = 1;

}

