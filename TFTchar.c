/*! \file  TFTchar.c
 *
 *  \brief Display text on the TFT
 *
 *
 *  \author jjmcd
 *  \date February 25, 2014, 8:18 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include <xc.h>
#include <math.h>
#include "HardwareDefs.h"
#include "MCP23S17defs.h"
#include "class_attributes.h"
#include "TFT.h"

#define byte uint8_t
#define word uint16_t
#define pgm_read_byte(data) *data
#define fontbyte(x) cfont.font[x]
#define LEFT 0
#define RIGHT 9999
#define CENTER 9998

/*! printChar - Print a single character */

/*!
 *
 */

void TFTprintChar(char c, int x, int y)
{
  char i,ch;
  word j;
  word temp;
  int zz;

  clearCS();

  if (!_transparent)
    {
      if (orient==PORTRAIT)
	{
	  setXY(x,y,x+cfont.x_size-1,y+cfont.y_size-1);

	  temp=((c-cfont.offset)*((cfont.x_size/8)*cfont.y_size))+4;
	  for(j=0;j<((cfont.x_size/8)*cfont.y_size);j++)
	    {
	      ch=pgm_read_byte(&cfont.font[temp]);
	      for(i=0;i<8;i++)
		{
		  if((ch&(1<<(7-i)))!=0)
		    {
		      //setPixel((fch<<8)|fcl);
		      setPixel( stForeground.fgcolor );
		    }
		  else
		    {
		      setPixel((bch<<8)|bcl);
		    }
		}
	      temp++;
	    }
	}
      else /* orient==LANDSCAPE */
	{
	  temp=((c-cfont.offset)*((cfont.x_size/8)*cfont.y_size))+4;

	  for(j=0;j<((cfont.x_size/8)*cfont.y_size);j+=(cfont.x_size/8))
	    {
	      setXY(x,y+(j/(cfont.x_size/8)),x+cfont.x_size-1,
		    y+(j/(cfont.x_size/8)));
	      for (zz=(cfont.x_size/8)-1; zz>=0; zz--)
		{
		  ch=pgm_read_byte(&cfont.font[temp+zz]);
		  for(i=0;i<8;i++)
		    {
		      if((ch&(1<<i))!=0)
			{
			  //setPixel((fch<<8)|fcl);
		          setPixel( stForeground.fgcolor );
			}
		      else
			{
			  setPixel((bch<<8)|bcl);
			}
		    }
		}
	      temp+=(cfont.x_size/8);
	    }
	}
    }
  else /* transparent */
    {
      temp=((c-cfont.offset)*((cfont.x_size/8)*cfont.y_size))+4;
      for(j=0;j<cfont.y_size;j++)
	{
	  for ( zz=0; zz<(cfont.x_size/8); zz++)
	    {
	      ch=pgm_read_byte(&cfont.font[temp+zz]);
	      for(i=0;i<8;i++)
		{
		  setXY(x+i+(zz*8),y+j,x+i+(zz*8)+1,y+j+1);

		  if((ch&(1<<(7-i)))!=0)
		    {
		      //setPixel((fch<<8)|fcl);
		      setPixel( stForeground.fgcolor );
		    }
		}
	    }
	  temp+=(cfont.x_size/8);
	}
    }
  setCS();
  clrXY();
}

/*! setFont - Select a font */

/*!
 *
 */
void TFTsetFont(const fontdatatype* font)
{
  cfont.font=(fontdatatype*)font;
  cfont.x_size=fontbyte(0);
  cfont.y_size=fontbyte(1);
  cfont.offset=fontbyte(2);
  cfont.numchars=fontbyte(3);
}

/*! rotateChar - Print a single character at an angle */

/*!
 *
 */
void TFTrotateChar(byte c, int x, int y, int pos, int deg)
{
  byte i,j,ch;
  word temp;
  int newx,newy;
  double radian;
  int zz;
  double srad, crad;

  radian=deg*0.0175;
  srad=sin(radian);
  crad=cos(radian);

  clearCS();

  temp=((c-cfont.offset)*((cfont.x_size/8)*cfont.y_size))+4;
  for(j=0;j<cfont.y_size;j++)
    {
      for (zz=0; zz<(cfont.x_size/8); zz++)
	{
	  ch=pgm_read_byte(&cfont.font[temp+zz]);
	  for(i=0;i<8;i++)
	    {
	      newx=x+(((i+(zz*8)+(pos*cfont.x_size))*crad)-((j)*srad));
	      newy=y+(((j)*crad)+((i+(zz*8)+(pos*cfont.x_size))*srad));

	      setXY(newx,newy,newx+1,newy+1);

	      if((ch&(1<<(7-i)))!=0)
		{
		  //setPixel((fch<<8)|fcl);
	          setPixel( stForeground.fgcolor );
		}
	      else
		{
		  if (!_transparent)
		    setPixel((bch<<8)|bcl);
		}
	    }
	}
      temp+=(cfont.x_size/8);
    }
  setCS();
  clrXY();
}


/*! print - Print a string */

/*!
 *
 */
void TFTprint(char *st, int x, int y, int deg)
{
  int stl, i;

  stl = strlen(st);

  if (orient==PORTRAIT)
    {
      if (x==RIGHT)
	x=(disp_x_size+1)-(stl*cfont.x_size);
      if (x==CENTER)
	x=((disp_x_size+1)-(stl*cfont.x_size))/2;
    }
  else
    {
      if (x==RIGHT)
	x=(disp_y_size+1)-(stl*cfont.x_size);
      if (x==CENTER)
	x=((disp_y_size+1)-(stl*cfont.x_size))/2;
    }

  for (i=0; i<stl; i++)
    if (deg==0)
      TFTprintChar(*st++, x + (i*(cfont.x_size)), y);
    else
      TFTrotateChar(*st++, x, y, i, deg);
}
