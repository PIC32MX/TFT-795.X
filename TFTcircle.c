/*! \file  TFTcircle.c
 *
 *  \brief Draw a circle
 *
 *
 *  \author jjmcd
 *  \date February 26, 2014, 7:49 AM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */
#include <xc.h>
//#include <math.h>
//#include "HardwareDefs.h"
//#include "MCP23S17defs.h"
#include "class_attributes.h"

#include "TFT.h"

/*! TFTcircle - Draw a circle on the TFT */

/*!
 *
 */
void TFTcircle(int x, int y, int radius)
{
        int f = 1 - radius;
        int ddF_x = 1;
        int ddF_y = -2 * radius;
        int x1 = 0;
        int y1 = radius;

        clearCS();
        setXY(x, y + radius, x, y + radius);
        //LCD_Write_DATA(fch,fcl);
        LCD_Write_DATA16( stForeground.fgcolor );

        setXY(x, y - radius, x, y - radius);
        //LCD_Write_DATA(fch,fcl);
        LCD_Write_DATA16( stForeground.fgcolor );
        setXY(x + radius, y, x + radius, y);
        //LCD_Write_DATA(fch,fcl);
        LCD_Write_DATA16( stForeground.fgcolor );
        setXY(x - radius, y, x - radius, y);
        //LCD_Write_DATA(fch,fcl);
        LCD_Write_DATA16( stForeground.fgcolor );

        while(x1 < y1)
        {
                if(f >= 0)
                {
                        y1--;
                        ddF_y += 2;
                        f += ddF_y;
                }
                x1++;
                ddF_x += 2;
                f += ddF_x;
                setXY(x + x1, y + y1, x + x1, y + y1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
                setXY(x - x1, y + y1, x - x1, y + y1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
                setXY(x + x1, y - y1, x + x1, y - y1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
                setXY(x - x1, y - y1, x - x1, y - y1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
                setXY(x + y1, y + x1, x + y1, y + x1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
                setXY(x - y1, y + x1, x - y1, y + x1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
                setXY(x + y1, y - x1, x + y1, y - x1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
                setXY(x - y1, y - x1, x - y1, y - x1);
                //LCD_Write_DATA(fch,fcl);
                LCD_Write_DATA16( stForeground.fgcolor );
        }
        setCS();
        clrXY();
}

void TFTfillCircle(int x, int y, int radius)
{
    int y1,x1;

        for(y1=-radius; y1<=0; y1++)
                for(x1=-radius; x1<=0; x1++)
                        if(x1*x1+y1*y1 <= radius*radius)
                        {
                                TFThLine(x+x1, y+y1, 2*(-x1));
                                TFThLine(x+x1, y-y1, 2*(-x1));
                                break;
                        }
}
