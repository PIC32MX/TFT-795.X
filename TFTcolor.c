/*! \file  TFTcolor.c
 *
 *  \brief Routines for setting the color to be used
 *
 *
 *  \author jjmcd
 *  \date January 8, 2014, 1:12 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include <stdint.h>
#include "class_attributes.h"
#include "TFT.h"


/*! TFTsetColor - Set the foregrounf color, RGB */

/*!
 *
 */

void TFTsetColor(uint8_t r, uint8_t g, uint8_t b)
{
        //fch=((r&248)|g>>5);
        stForeground.rgbPart.fchi=((r&248)|g>>5);
        //fcl=((g&28)<<3|b>>3);
        stForeground.rgbPart.fclo=((g&28)<<3|b>>3);
}

/*! TFTsetColorX - Set the foreground color, 16-bit */

/*!
 *
 */
void TFTsetColorX( unsigned int x )
{
//    fch = (x&0xff00)>>8;
//    fcl = x & 0xff;
    stForeground.fgcolor=x;
}

/*! TFTsetBackColor - Set the background color, RGB */

/*!
 *
 */
void TFTsetBackColor(uint8_t r, uint8_t g, uint8_t b)
{
        bch=((r&248)|g>>5);
        bcl=((g&28)<<3|b>>3);
        _transparent=false;
}

/*! TFTsetBackColorX - Set the background color, 16-bit */

/*!
 *
 */
void TFTsetBackColorX(unsigned int x)
{
    bch = (x&0xff00)>>8;
    bcl = x & 0xff;
    _transparent=false;
}

/*! TFTsetTransparent - Set font drawing background transparent */

/*!
 *
 */
void TFTsetTransparent( int trans )
{
    if ( trans )
        _transparent = 1;
    else
        _transparent = 0;
}

/*! TFTgetBackColor - Return the current 16-bit background color */
unsigned int TFTgetBackColor( void )
{
    return bch<<8 | bcl;
}

/*! TFTgetColor - Return the current 16-bit foreground color */
unsigned int TFTgetColor( void )
{
    //return fch<<8 | fcl;
    return stForeground.fgcolor;
}

