/*! \file  TFTeraseRect.c
 *
 *  \brief Clear a rectangle to the background color
 *
 *
 *  \author jjmcd
 *  \date June 26, 2015, 7:33 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include "HardwareDefs.h"
#include "MCP23S17defs.h"
#include "class_attributes.h"
#include "TFT.h"

/*! main - */

/*!
 *
 */
void TFTeraseRect(int x1,int y1,int x2,int y2)
{
  uint16_t colval;
  long i,n;

  n = ( (long)x2-(long)x1+1L ) * ( (long)y2-(long)y1+1L );
  colval = ( bch<<8 ) | bcl ;
  clearCS();
  setXY(x1,y1,x2,y2);
  setRS();
  sendToS17(0,S17_OLATA,colval);
  for ( i=0; i<n; i++ )
    {
      T_WR = 0;
      //Nop();
      T_WR = 1;
      //Nop();
    }
  setCS();
}
