/*! \file  TFTprintDec.c
 *
 *  \brief Print a number with an inserted decimal point
 *
 *
 *  \author jjmcd
 *  \date March 29, 2014, 7:15 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "TFT.h"

/* K&R p.62 */
void reverse( char *s )
{
  int c, i, j;

  for ( i=0, j=strlen(s)-1; i<j; i++, j-- )
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
    }
}

void insertDec( char *szBuffer, int dec )
{
  int n;
  char szWork[32];

  if ( dec<0 )
    {
      strcpy(szBuffer,"**ERROR**");
      return;
    }
  n=strlen(szBuffer);
  reverse(szBuffer);
  while ( n<=dec )
    {
      strcat(szBuffer,"0");
      n++;
    }
  reverse(szBuffer);
  // Apparently, PIC32's strcpy isn't like gcc's
  //strcpy(&szBuffer[n-dec+1],&szBuffer[n-dec]);
  //szBuffer[n-dec]='.';
  memset(szWork,0,sizeof(szWork));
  strcpy(szWork,szBuffer);
  szWork[n-dec]='.';
  strcpy(&szWork[n-dec+1],&szBuffer[n-dec]);
  strcpy(szBuffer,szWork);
}


void printRight(char *szBuffer,int x,int y)
{
    x-=(8*strlen(szBuffer));
    TFTprint(szBuffer,x+1,y,0);
}


/*! TFTprintDec - Print an integer with inserted decimal */

/*! Display an integer on the screen with an inserted decimal point
 *
 * \param val int - value to be printed
 * \param dec int - number of places from the right to place the decimal
 * \param x int - horizontal position of the number on the screen
 * \param y int - vertical position of the number on the screen
 */
void TFTprintDec( int val, int dec, int x, int y )
{
  char szBuffer[32];

  itoa(szBuffer,val,10);
  insertDec(szBuffer,dec);

  TFTprint(szBuffer,x,y,0);
}

/*! TFTprintDec - Print a right-justified integer with inserted decimal */

/*! Display an integer on the screen with an inserted decimal point
 *  right-justified
 *
 * \param val int - value to be printed
 * \param dec int - number of places from the right to place the decimal
 * \param x int - horizontal position of the right end of the number on the screen
 * \param y int - vertical position of the number on the screen
 */
void TFTprintDecRight( int val, int dec, int x, int y )
{
  char szBuffer[32];

  itoa(szBuffer,val,10);
  insertDec(szBuffer,dec);

  printRight(szBuffer,x,y);
}
