/*! \file  TFTprintInt.c
 *
 *  \brief Print an integer
 *
 *
 *  \author jjmcd
 *  \date April 19, 2014, 6:47 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>

void printRight(char *,int,int);

#define PRINTINTHEX 0x0001
#define PRINTINTRIGHT 0x0002
#define PRINTINTZERO 0x0004

/*! TFTprintInt - */

/*! Print an integer
 *
 */
void TFTprintInt(int n, unsigned int options, int x, int y )
{
    char szBuffer[64];
    int nZeros;
    int i;

    memset(szBuffer,0,sizeof(szBuffer));
    if ( options & PRINTINTHEX )
    {
        itoa(szBuffer,n,16);
        nZeros=4;
    }
    else
    {
        itoa(szBuffer,n,10);
        nZeros=5;
    }

    if ( options & PRINTINTZERO )
        {
            for ( i=strlen(szBuffer); i>=0; i-- )
                szBuffer[i+1]=szBuffer[i];
            szBuffer[0]='0';
        }

    if ( options & PRINTINTRIGHT )
        printRight(szBuffer,x,y);
    else
        TFTprint(szBuffer,x,y,0);

}
