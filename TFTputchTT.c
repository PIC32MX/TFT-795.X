/*! \file  TFTputchTT.c
 *
 *  \brief Display a character, teletype mode
 *
 *
 *  \author jjmcd
 *  \date June 22, 2015, 7:40 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFT.h"

int nCurFont;
int nCurRow, nCurCol;
int nTTn1, nTTn2, nTTstate;
int nFontRowSize[7] = {12, 12, 16, 50, 16, 16, 16};
int nFontColSize[7] = {6, 6, 12, 32, 8, 8, 8};

/*! TFTputchTT - Display characters like a teletype */

/*!
 *
 */
void TFTputchTT(unsigned char ch)
{
  switch (nTTstate)
    {
      case 0:
        break;
      case 1:
        if (ch == '[')
          nTTstate = 2;
        return;
        break;
      case 2:
        if (ch == 'H')
          {
            nCurRow = nCurCol = 0;
            nTTstate = 0;
            return;
          }
        if ((ch > 0x2f) && (ch < 0x3a))
          {
            nTTn1 = nTTn1 * 10 + (ch & 0x0f);
            nTTstate = 3;
            return;
          }
        if (ch == 'K') // Erase from cursor to end of line
          {
            TFTeraseRect(nCurCol,
                    nCurRow,
                    319,
                    nCurRow + nFontRowSize[nCurFont]);
            nTTstate = 0;
            return;
          }
        if (ch == 'J') // Erase from cursor to end of screen
          {
            TFTeraseRect(nCurCol,
                    nCurRow,
                    319,
                    nCurRow + nFontRowSize[nCurFont]);
            TFTeraseRect(0, nCurRow + nFontRowSize[nCurFont] + 1, 319, 239);
            nTTstate = 0;
            return;
          }
      case 3:
        if ((ch > 0x2f) && (ch < 0x3a))
          {
            nTTn1 = nTTn1 * 10 + (ch & 0x0f);
            return;
          }
        if (ch == ';')
          {
            nTTstate = 4;
            return;
          }
      case 4:
        if ((ch > 0x2f) && (ch < 0x3a))
          {
            nTTn2 = nTTn2 * 10 + (ch & 0x0f);
            return;
          }
        if (ch == 'H')
          {
            if (nCurRow == 0)
              nCurCol = 1;
            if (nCurRow == 0)
              nCurCol = 1;
            nCurRow = (nTTn1 - 1) * nFontRowSize[nCurFont];
            nCurCol = (nTTn2 - 1) * nFontColSize[nCurFont];
            nTTstate = 0;
            return;
          }
        if (ch == 'K')
          {
            switch (nTTn1)
              {
                case 1: // Erase from beginning of line to cursor
                  TFTeraseRect(0, nCurRow, nCurCol + nFontColSize[nCurFont] - 1,
                          nCurRow + nFontRowSize[nCurFont]);
                  nTTstate = 0;
                  return;
                case 2: // Erase entire line containing cursor
                  TFTeraseRect(0, nCurRow, 319, nCurRow + nFontRowSize[nCurFont]);
                  nTTstate = 0;
                  return;
                default: // Erase from cursor to end of line
                  TFTeraseRect(nCurCol, nCurRow,
                          319, nCurRow + nFontRowSize[nCurFont]);
                  nTTstate = 0;
                  return;
              }
          }
        if (ch == 'J')
          {
            switch (nTTn1)
              {
                case 1: // Erase from beginning of screen to cursor
                  TFTeraseRect(0, 0, 319, nCurRow - 1);
                  TFTeraseRect(0, nCurRow, nCurCol + nFontColSize[nCurFont] - 1,
                          nCurRow + nFontRowSize[nCurFont]);
                  nTTstate = 0;
                  return;
                case 2: // Erase entire screen
                  TFTeraseRect(0, 0, 319, 239);
                  nTTstate = 0;
                  return;
                default: // Erase from cursor to end of screen
                  TFTeraseRect(nCurCol,
                          nCurRow,
                          319,
                          nCurRow + nFontRowSize[nCurFont]);
                  TFTeraseRect(0, nCurRow + nFontRowSize[nCurFont] + 1, 319, 239);
                  nTTstate = 0;
                  return;
              }
          }
      case 5:
      case 6:
      case 7:
      default:
        return;
        break;
    }
  if (ch == 0x1b)
    {
      nTTstate = 1;
      nTTn1 = nTTn2 = 0;
      return;
    }
  /* If past the right of the screen, wrap to left */
  if (nCurCol > 319)
    {
      nCurCol = 0;
      /* Next line */
      nCurRow += nFontRowSize[nCurFont];
      /* If off the bottom of the screen, wrap to top */
      if (nCurRow > 239)
        nCurRow = 0;
    }

  /* If it's a printable character */
  if ((ch > 0x1f) && (ch < 0x80))
    {
      if ((nCurFont != 3) || ((ch > '!') && (ch < ':')))
        {
          /* Display the character at the current position */
          TFTprintChar(ch, nCurCol, nCurRow);
          /* Move to next row */
          nCurCol += nFontColSize[nCurFont];
        }
    }
  /* If the character is a line feed */
  if (ch == 0x0a)
    {
      /* Move to next row */
      nCurRow += nFontRowSize[nCurFont];
      /* If off the bottom of the screen, wrap to top */
      if (nCurRow > 239)
        nCurRow = 0;
    }
  /* If the character is a carriage return */
  if (ch == 0x0d)
    /* Move to left column */
    nCurCol = 0;
  /* If the character is a form feed */
  if (ch == 0x0c)
    {
      /* Clear the screen */
      TFTclear();
      /* And set the column, row to the top */
      nCurCol = nCurRow = 0;
    }
  nTTn1 = nTTn2 = nTTstate = 0;
}
