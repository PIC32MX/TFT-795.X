/*! \file  TFTrect.c
 *
 *  \brief Draw a rectangle
 *
 *
 *  \author jjmcd
 *  \date March 21, 2014, 2:31 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdint.h>
#include "HardwareDefs.h"
#include "MCP23S17defs.h"
#include "class_attributes.h"
#include "TFT.h"

#define swap(type, i, j) {type t = i; i = j; j = t;}

/*! TFTrect - Draw a rectangle */

/*!
 *
 */
void TFTrect(int x1, int y1, int x2, int y2)
{
        int tmp;

        if (x1>x2)
        {
                swap(int, x1, x2);
        }
        if (y1>y2)
        {
                swap(int, y1, y2);
        }

        TFThLine(x1, y1, x2-x1);
        TFThLine(x1, y2, x2-x1);
        TFTvLine(x1, y1, y2-y1);
        TFTvLine(x2, y1, y2-y1);
}

void TFTfillRect(int x1,int y1,int x2,int y2)
{
  uint16_t colval;
  long i,n;
  
  if (x1>x2)
    {
      swap(int, x1, x2);
    }
  if (y1>y2)
    {
      swap(int, y1, y2);
    }

  n = ( (long)x2-(long)x1+1L ) * ( (long)y2-(long)y1+1L );
  //colval = fch<<8 | fcl;
  colval = stForeground.fgcolor;
  clearCS();
  setXY(x1,y1,x2,y2);
  setRS();
  sendToS17(0,S17_OLATA,colval);
  //sendToS17(0,S17_OLATA,0xffff);
  for ( i=0; i<n; i++ )
    {
      T_WR = 0;
      T_WR = 1;
    }
  clrXY();
  setCS();
}
