/*! \file  bitsets.c
 *
 *  \brief Set and clear various TFT control lines
 *
 * This file contains a number of functions related to setting
 * or clearing various control lines.  Making these individual
 * functions permits avoiding unnecessary SPI transactions. The
 * WR (write) bit is set individually outside these functions.
 *
 * Functions:
 *  \li setRS() - Set the register select bit
 *  \li clearRS() - Clear the register select bit
 *  \li setCS() - Set the chip select bit
 *  \li clearCS() - Clear the chip select bit
 *  \li setRD() - Set the read bit
 *  \li clearRD() - Clear the read bit
 *  \li setRST() - Set the reset bit
 *  \li clearRST() - Clear the reset bit
 *  \li setClear() - Actually send the changes to the MCP23S17
 *
 *  \author jjmcd
 *  \date January 2, 2014, 2:59 PM
 *
 * \todo Add macros for set/clear WR BZ#557
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */
#include <xc.h>
#include "MCP23S17defs.h"
#include "HardwareDefs.h"
/*! The class attributes are implemented in this module */
/*! The EXTERN macro is defined as extern in class_attributes.h if not
 *  previously defined.  Any module including class_attributes.h will then
 *  have the variables defined therein as extern.  However, should EXTERN
 *  be defined before including class_attributes.h the variables will
 *  actually be implemented.
 */
#define EXTERN
#include "class_attributes.h"

/*! setClear - Set or clear bits controlled by MCP23S17 */
/*! setClear() tests to see whether the new contents of the
 *  second MCP23S17 are different than the previous.  If so, it
 *  sends the new contents and remembers that value as the new
 *  old contents.
 *
 *  \param mask uint16_t - Desired contents of the I/O expander latch
 *  \returns none
 */
void setClear( uint16_t mask )
{
     if ( mask != IOXold )
    {
        sendToS17(1,S17_OLATA,mask);
        IOXold = mask;
    }

}

/*! clearRS - Clear the TFT's Register Select Bit */
/*! clearRS() simply sets the port latch tied to (PORT B, bit 14)
 *  to zero.
 *
 * \param none
 * \returns none
 *
 * \todo Make this a macro BZ#556
 */
void clearRS( void )
{
  T_RS = 0;
}

/*! clearCS - Clear the TFT's Chip Select Bit */
/*! clearCS() clears the T_CS bit in the previous mask and
 *  sends the result to setClear().
 *
 * \param none
 * \returns none
 */
void clearCS( void )
{
    IOXnew = IOXold & T_CS_OFM;
    setClear( IOXnew );
}

/*! clearRD - Clear the TFT's Read Bit */
/*! clearRD() clears the T_RD bit in the previous mask and
 *  sends the result to setClear().
 *
 * \param none
 * \returns none
 */
void clearRD( void )
{
    IOXnew = IOXold & T_RD_OFM;
    setClear( IOXnew );
}

/*! clearRST - Clear the TFT's Reset Bit */
/*! clearRST() clears the T_RST bit in the previous mask and
 *  sends the result to setClear().
 *
 * \param none
 * \returns none
 */
void clearRST( void )
{
    IOXnew = IOXold & T_RST_OFM;
    setClear( IOXnew );
}


/*! setRS - Set the TFT's Register Select Bit */
/*! setRS() simply sets the port latch tied to (PORT B, bit 14)
 *  to one.
 *
 * \param none
 * \returns none
 *
 * \todo Make this a macro BZ#556
 */
void setRS( void )
{
  T_RS = 1;
}

/*! setCS - Clear the TFT's Chip Select Bit */
/*! setCS() sets the T_CS bit in the previous mask and
 *  sends the result to setClear().
 *
 * \param none
 * \returns none
 */
void setCS( void )
{
    IOXnew = IOXold | T_CS_ONM;
    setClear( IOXnew );
}

/*! setRD - Clear the TFT's Read Bit */
/*! setRD() sets the T_RD bit in the previous mask and
 *  sends the result to setClear().
 *
 * \param none
 * \returns none
 */
void setRD( void )
{
    IOXnew = IOXold | T_RD_ONM;
    setClear( IOXnew );
}

/*! setRST - Clear the TFT's Reset Bit */
/*! setRST() sets the T_RST bit in the previous mask and
 *  sends the result to setClear().
 *
 * \param none
 * \returns none
 */
void setRST( void )
{
    IOXnew = IOXold | T_RST_ONM;
    setClear( IOXnew );

}
