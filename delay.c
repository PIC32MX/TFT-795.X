/*! \file  delay.c
 *
 *  \brief Delay by looping
 *
 *
 *  \author jjmcd
 *  \date January 8, 2014, 1:17 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

/*! Delay constant for 50 MHz clock */
#define DELAYCOUNTER 4189

/*! delay - Delay for 1*l milliseconds */

/*! Loop for a specified number of milliseconds
 *
 * \param l long - Number of millisedonds to delay
 * \returns none
 *
 */
void delay( unsigned int l )
{
    long i;

    while ( l-- )
        for ( i=0; i<DELAYCOUNTER; i++ )
            ;

}
