#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/TFT.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/TFT.X.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=DefaultFonts.c LCD_Writ_Bus.c LCD_Write_COM.c LCD_Write_DATA.c SetupHardware.c TFTclear.c TFTcolor.c TFTline.c TFTroundRect.c bitsets.c delay.c sendToS17.c waitAwhile.c TFTchar.c TFTinit.c setXY.c TFTeraseRect.c TFTpixel.c TFTcircle.c TFTrect.c TFTprintDec.c DJS.c DJSB.c TFTprintInt.c TFTtransitions.c TFTputchTT.c TFTputsTT.c TFTGaddPoint.c TFTGannotate.c TFTGcolors.c TFTGexpose.c TFTGinit.c TFTGlabels.c TFTGnoCursor.c TFTGnoErase.c TFTGrange.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/DefaultFonts.o ${OBJECTDIR}/LCD_Writ_Bus.o ${OBJECTDIR}/LCD_Write_COM.o ${OBJECTDIR}/LCD_Write_DATA.o ${OBJECTDIR}/SetupHardware.o ${OBJECTDIR}/TFTclear.o ${OBJECTDIR}/TFTcolor.o ${OBJECTDIR}/TFTline.o ${OBJECTDIR}/TFTroundRect.o ${OBJECTDIR}/bitsets.o ${OBJECTDIR}/delay.o ${OBJECTDIR}/sendToS17.o ${OBJECTDIR}/waitAwhile.o ${OBJECTDIR}/TFTchar.o ${OBJECTDIR}/TFTinit.o ${OBJECTDIR}/setXY.o ${OBJECTDIR}/TFTeraseRect.o ${OBJECTDIR}/TFTpixel.o ${OBJECTDIR}/TFTcircle.o ${OBJECTDIR}/TFTrect.o ${OBJECTDIR}/TFTprintDec.o ${OBJECTDIR}/DJS.o ${OBJECTDIR}/DJSB.o ${OBJECTDIR}/TFTprintInt.o ${OBJECTDIR}/TFTtransitions.o ${OBJECTDIR}/TFTputchTT.o ${OBJECTDIR}/TFTputsTT.o ${OBJECTDIR}/TFTGaddPoint.o ${OBJECTDIR}/TFTGannotate.o ${OBJECTDIR}/TFTGcolors.o ${OBJECTDIR}/TFTGexpose.o ${OBJECTDIR}/TFTGinit.o ${OBJECTDIR}/TFTGlabels.o ${OBJECTDIR}/TFTGnoCursor.o ${OBJECTDIR}/TFTGnoErase.o ${OBJECTDIR}/TFTGrange.o
POSSIBLE_DEPFILES=${OBJECTDIR}/DefaultFonts.o.d ${OBJECTDIR}/LCD_Writ_Bus.o.d ${OBJECTDIR}/LCD_Write_COM.o.d ${OBJECTDIR}/LCD_Write_DATA.o.d ${OBJECTDIR}/SetupHardware.o.d ${OBJECTDIR}/TFTclear.o.d ${OBJECTDIR}/TFTcolor.o.d ${OBJECTDIR}/TFTline.o.d ${OBJECTDIR}/TFTroundRect.o.d ${OBJECTDIR}/bitsets.o.d ${OBJECTDIR}/delay.o.d ${OBJECTDIR}/sendToS17.o.d ${OBJECTDIR}/waitAwhile.o.d ${OBJECTDIR}/TFTchar.o.d ${OBJECTDIR}/TFTinit.o.d ${OBJECTDIR}/setXY.o.d ${OBJECTDIR}/TFTeraseRect.o.d ${OBJECTDIR}/TFTpixel.o.d ${OBJECTDIR}/TFTcircle.o.d ${OBJECTDIR}/TFTrect.o.d ${OBJECTDIR}/TFTprintDec.o.d ${OBJECTDIR}/DJS.o.d ${OBJECTDIR}/DJSB.o.d ${OBJECTDIR}/TFTprintInt.o.d ${OBJECTDIR}/TFTtransitions.o.d ${OBJECTDIR}/TFTputchTT.o.d ${OBJECTDIR}/TFTputsTT.o.d ${OBJECTDIR}/TFTGaddPoint.o.d ${OBJECTDIR}/TFTGannotate.o.d ${OBJECTDIR}/TFTGcolors.o.d ${OBJECTDIR}/TFTGexpose.o.d ${OBJECTDIR}/TFTGinit.o.d ${OBJECTDIR}/TFTGlabels.o.d ${OBJECTDIR}/TFTGnoCursor.o.d ${OBJECTDIR}/TFTGnoErase.o.d ${OBJECTDIR}/TFTGrange.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/DefaultFonts.o ${OBJECTDIR}/LCD_Writ_Bus.o ${OBJECTDIR}/LCD_Write_COM.o ${OBJECTDIR}/LCD_Write_DATA.o ${OBJECTDIR}/SetupHardware.o ${OBJECTDIR}/TFTclear.o ${OBJECTDIR}/TFTcolor.o ${OBJECTDIR}/TFTline.o ${OBJECTDIR}/TFTroundRect.o ${OBJECTDIR}/bitsets.o ${OBJECTDIR}/delay.o ${OBJECTDIR}/sendToS17.o ${OBJECTDIR}/waitAwhile.o ${OBJECTDIR}/TFTchar.o ${OBJECTDIR}/TFTinit.o ${OBJECTDIR}/setXY.o ${OBJECTDIR}/TFTeraseRect.o ${OBJECTDIR}/TFTpixel.o ${OBJECTDIR}/TFTcircle.o ${OBJECTDIR}/TFTrect.o ${OBJECTDIR}/TFTprintDec.o ${OBJECTDIR}/DJS.o ${OBJECTDIR}/DJSB.o ${OBJECTDIR}/TFTprintInt.o ${OBJECTDIR}/TFTtransitions.o ${OBJECTDIR}/TFTputchTT.o ${OBJECTDIR}/TFTputsTT.o ${OBJECTDIR}/TFTGaddPoint.o ${OBJECTDIR}/TFTGannotate.o ${OBJECTDIR}/TFTGcolors.o ${OBJECTDIR}/TFTGexpose.o ${OBJECTDIR}/TFTGinit.o ${OBJECTDIR}/TFTGlabels.o ${OBJECTDIR}/TFTGnoCursor.o ${OBJECTDIR}/TFTGnoErase.o ${OBJECTDIR}/TFTGrange.o

# Source Files
SOURCEFILES=DefaultFonts.c LCD_Writ_Bus.c LCD_Write_COM.c LCD_Write_DATA.c SetupHardware.c TFTclear.c TFTcolor.c TFTline.c TFTroundRect.c bitsets.c delay.c sendToS17.c waitAwhile.c TFTchar.c TFTinit.c setXY.c TFTeraseRect.c TFTpixel.c TFTcircle.c TFTrect.c TFTprintDec.c DJS.c DJSB.c TFTprintInt.c TFTtransitions.c TFTputchTT.c TFTputsTT.c TFTGaddPoint.c TFTGannotate.c TFTGcolors.c TFTGexpose.c TFTGinit.c TFTGlabels.c TFTGnoCursor.c TFTGnoErase.c TFTGrange.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/TFT.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX150F128B
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/DefaultFonts.o: DefaultFonts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DefaultFonts.o.d 
	@${RM} ${OBJECTDIR}/DefaultFonts.o 
	@${FIXDEPS} "${OBJECTDIR}/DefaultFonts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DefaultFonts.o.d" -o ${OBJECTDIR}/DefaultFonts.o DefaultFonts.c    -Wextra
	
${OBJECTDIR}/LCD_Writ_Bus.o: LCD_Writ_Bus.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD_Writ_Bus.o.d 
	@${RM} ${OBJECTDIR}/LCD_Writ_Bus.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD_Writ_Bus.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCD_Writ_Bus.o.d" -o ${OBJECTDIR}/LCD_Writ_Bus.o LCD_Writ_Bus.c    -Wextra
	
${OBJECTDIR}/LCD_Write_COM.o: LCD_Write_COM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD_Write_COM.o.d 
	@${RM} ${OBJECTDIR}/LCD_Write_COM.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD_Write_COM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCD_Write_COM.o.d" -o ${OBJECTDIR}/LCD_Write_COM.o LCD_Write_COM.c    -Wextra
	
${OBJECTDIR}/LCD_Write_DATA.o: LCD_Write_DATA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD_Write_DATA.o.d 
	@${RM} ${OBJECTDIR}/LCD_Write_DATA.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD_Write_DATA.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCD_Write_DATA.o.d" -o ${OBJECTDIR}/LCD_Write_DATA.o LCD_Write_DATA.c    -Wextra
	
${OBJECTDIR}/SetupHardware.o: SetupHardware.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SetupHardware.o.d 
	@${RM} ${OBJECTDIR}/SetupHardware.o 
	@${FIXDEPS} "${OBJECTDIR}/SetupHardware.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/SetupHardware.o.d" -o ${OBJECTDIR}/SetupHardware.o SetupHardware.c    -Wextra
	
${OBJECTDIR}/TFTclear.o: TFTclear.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTclear.o.d 
	@${RM} ${OBJECTDIR}/TFTclear.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTclear.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTclear.o.d" -o ${OBJECTDIR}/TFTclear.o TFTclear.c    -Wextra
	
${OBJECTDIR}/TFTcolor.o: TFTcolor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTcolor.o.d 
	@${RM} ${OBJECTDIR}/TFTcolor.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTcolor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTcolor.o.d" -o ${OBJECTDIR}/TFTcolor.o TFTcolor.c    -Wextra
	
${OBJECTDIR}/TFTline.o: TFTline.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTline.o.d 
	@${RM} ${OBJECTDIR}/TFTline.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTline.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTline.o.d" -o ${OBJECTDIR}/TFTline.o TFTline.c    -Wextra
	
${OBJECTDIR}/TFTroundRect.o: TFTroundRect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTroundRect.o.d 
	@${RM} ${OBJECTDIR}/TFTroundRect.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTroundRect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTroundRect.o.d" -o ${OBJECTDIR}/TFTroundRect.o TFTroundRect.c    -Wextra
	
${OBJECTDIR}/bitsets.o: bitsets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/bitsets.o.d 
	@${RM} ${OBJECTDIR}/bitsets.o 
	@${FIXDEPS} "${OBJECTDIR}/bitsets.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/bitsets.o.d" -o ${OBJECTDIR}/bitsets.o bitsets.c    -Wextra
	
${OBJECTDIR}/delay.o: delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/delay.o.d 
	@${RM} ${OBJECTDIR}/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/delay.o.d" -o ${OBJECTDIR}/delay.o delay.c    -Wextra
	
${OBJECTDIR}/sendToS17.o: sendToS17.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sendToS17.o.d 
	@${RM} ${OBJECTDIR}/sendToS17.o 
	@${FIXDEPS} "${OBJECTDIR}/sendToS17.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/sendToS17.o.d" -o ${OBJECTDIR}/sendToS17.o sendToS17.c    -Wextra
	
${OBJECTDIR}/waitAwhile.o: waitAwhile.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/waitAwhile.o.d 
	@${RM} ${OBJECTDIR}/waitAwhile.o 
	@${FIXDEPS} "${OBJECTDIR}/waitAwhile.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/waitAwhile.o.d" -o ${OBJECTDIR}/waitAwhile.o waitAwhile.c    -Wextra
	
${OBJECTDIR}/TFTchar.o: TFTchar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTchar.o.d 
	@${RM} ${OBJECTDIR}/TFTchar.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTchar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTchar.o.d" -o ${OBJECTDIR}/TFTchar.o TFTchar.c    -Wextra
	
${OBJECTDIR}/TFTinit.o: TFTinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTinit.o.d 
	@${RM} ${OBJECTDIR}/TFTinit.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTinit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTinit.o.d" -o ${OBJECTDIR}/TFTinit.o TFTinit.c    -Wextra
	
${OBJECTDIR}/setXY.o: setXY.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/setXY.o.d 
	@${RM} ${OBJECTDIR}/setXY.o 
	@${FIXDEPS} "${OBJECTDIR}/setXY.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/setXY.o.d" -o ${OBJECTDIR}/setXY.o setXY.c    -Wextra
	
${OBJECTDIR}/TFTeraseRect.o: TFTeraseRect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTeraseRect.o.d 
	@${RM} ${OBJECTDIR}/TFTeraseRect.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTeraseRect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTeraseRect.o.d" -o ${OBJECTDIR}/TFTeraseRect.o TFTeraseRect.c    -Wextra
	
${OBJECTDIR}/TFTpixel.o: TFTpixel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTpixel.o.d 
	@${RM} ${OBJECTDIR}/TFTpixel.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTpixel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTpixel.o.d" -o ${OBJECTDIR}/TFTpixel.o TFTpixel.c    -Wextra
	
${OBJECTDIR}/TFTcircle.o: TFTcircle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTcircle.o.d 
	@${RM} ${OBJECTDIR}/TFTcircle.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTcircle.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTcircle.o.d" -o ${OBJECTDIR}/TFTcircle.o TFTcircle.c    -Wextra
	
${OBJECTDIR}/TFTrect.o: TFTrect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTrect.o.d 
	@${RM} ${OBJECTDIR}/TFTrect.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTrect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTrect.o.d" -o ${OBJECTDIR}/TFTrect.o TFTrect.c    -Wextra
	
${OBJECTDIR}/TFTprintDec.o: TFTprintDec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTprintDec.o.d 
	@${RM} ${OBJECTDIR}/TFTprintDec.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTprintDec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTprintDec.o.d" -o ${OBJECTDIR}/TFTprintDec.o TFTprintDec.c    -Wextra
	
${OBJECTDIR}/DJS.o: DJS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DJS.o.d 
	@${RM} ${OBJECTDIR}/DJS.o 
	@${FIXDEPS} "${OBJECTDIR}/DJS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DJS.o.d" -o ${OBJECTDIR}/DJS.o DJS.c    -Wextra
	
${OBJECTDIR}/DJSB.o: DJSB.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DJSB.o.d 
	@${RM} ${OBJECTDIR}/DJSB.o 
	@${FIXDEPS} "${OBJECTDIR}/DJSB.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DJSB.o.d" -o ${OBJECTDIR}/DJSB.o DJSB.c    -Wextra
	
${OBJECTDIR}/TFTprintInt.o: TFTprintInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTprintInt.o.d 
	@${RM} ${OBJECTDIR}/TFTprintInt.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTprintInt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTprintInt.o.d" -o ${OBJECTDIR}/TFTprintInt.o TFTprintInt.c    -Wextra
	
${OBJECTDIR}/TFTtransitions.o: TFTtransitions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTtransitions.o.d 
	@${RM} ${OBJECTDIR}/TFTtransitions.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTtransitions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTtransitions.o.d" -o ${OBJECTDIR}/TFTtransitions.o TFTtransitions.c    -Wextra
	
${OBJECTDIR}/TFTputchTT.o: TFTputchTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTputchTT.o.d 
	@${RM} ${OBJECTDIR}/TFTputchTT.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTputchTT.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTputchTT.o.d" -o ${OBJECTDIR}/TFTputchTT.o TFTputchTT.c    -Wextra
	
${OBJECTDIR}/TFTputsTT.o: TFTputsTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTputsTT.o.d 
	@${RM} ${OBJECTDIR}/TFTputsTT.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTputsTT.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTputsTT.o.d" -o ${OBJECTDIR}/TFTputsTT.o TFTputsTT.c    -Wextra
	
${OBJECTDIR}/TFTGaddPoint.o: TFTGaddPoint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o.d 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGaddPoint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGaddPoint.o.d" -o ${OBJECTDIR}/TFTGaddPoint.o TFTGaddPoint.c    -Wextra
	
${OBJECTDIR}/TFTGannotate.o: TFTGannotate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGannotate.o.d 
	@${RM} ${OBJECTDIR}/TFTGannotate.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGannotate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGannotate.o.d" -o ${OBJECTDIR}/TFTGannotate.o TFTGannotate.c    -Wextra
	
${OBJECTDIR}/TFTGcolors.o: TFTGcolors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGcolors.o.d 
	@${RM} ${OBJECTDIR}/TFTGcolors.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGcolors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGcolors.o.d" -o ${OBJECTDIR}/TFTGcolors.o TFTGcolors.c    -Wextra
	
${OBJECTDIR}/TFTGexpose.o: TFTGexpose.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGexpose.o.d 
	@${RM} ${OBJECTDIR}/TFTGexpose.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGexpose.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGexpose.o.d" -o ${OBJECTDIR}/TFTGexpose.o TFTGexpose.c    -Wextra
	
${OBJECTDIR}/TFTGinit.o: TFTGinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGinit.o.d 
	@${RM} ${OBJECTDIR}/TFTGinit.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGinit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGinit.o.d" -o ${OBJECTDIR}/TFTGinit.o TFTGinit.c    -Wextra
	
${OBJECTDIR}/TFTGlabels.o: TFTGlabels.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGlabels.o.d 
	@${RM} ${OBJECTDIR}/TFTGlabels.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGlabels.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGlabels.o.d" -o ${OBJECTDIR}/TFTGlabels.o TFTGlabels.c    -Wextra
	
${OBJECTDIR}/TFTGnoCursor.o: TFTGnoCursor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGnoCursor.o.d 
	@${RM} ${OBJECTDIR}/TFTGnoCursor.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGnoCursor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGnoCursor.o.d" -o ${OBJECTDIR}/TFTGnoCursor.o TFTGnoCursor.c    -Wextra
	
${OBJECTDIR}/TFTGnoErase.o: TFTGnoErase.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGnoErase.o.d 
	@${RM} ${OBJECTDIR}/TFTGnoErase.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGnoErase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGnoErase.o.d" -o ${OBJECTDIR}/TFTGnoErase.o TFTGnoErase.c    -Wextra
	
${OBJECTDIR}/TFTGrange.o: TFTGrange.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGrange.o.d 
	@${RM} ${OBJECTDIR}/TFTGrange.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGrange.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGrange.o.d" -o ${OBJECTDIR}/TFTGrange.o TFTGrange.c    -Wextra
	
else
${OBJECTDIR}/DefaultFonts.o: DefaultFonts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DefaultFonts.o.d 
	@${RM} ${OBJECTDIR}/DefaultFonts.o 
	@${FIXDEPS} "${OBJECTDIR}/DefaultFonts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DefaultFonts.o.d" -o ${OBJECTDIR}/DefaultFonts.o DefaultFonts.c    -Wextra
	
${OBJECTDIR}/LCD_Writ_Bus.o: LCD_Writ_Bus.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD_Writ_Bus.o.d 
	@${RM} ${OBJECTDIR}/LCD_Writ_Bus.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD_Writ_Bus.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCD_Writ_Bus.o.d" -o ${OBJECTDIR}/LCD_Writ_Bus.o LCD_Writ_Bus.c    -Wextra
	
${OBJECTDIR}/LCD_Write_COM.o: LCD_Write_COM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD_Write_COM.o.d 
	@${RM} ${OBJECTDIR}/LCD_Write_COM.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD_Write_COM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCD_Write_COM.o.d" -o ${OBJECTDIR}/LCD_Write_COM.o LCD_Write_COM.c    -Wextra
	
${OBJECTDIR}/LCD_Write_DATA.o: LCD_Write_DATA.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCD_Write_DATA.o.d 
	@${RM} ${OBJECTDIR}/LCD_Write_DATA.o 
	@${FIXDEPS} "${OBJECTDIR}/LCD_Write_DATA.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCD_Write_DATA.o.d" -o ${OBJECTDIR}/LCD_Write_DATA.o LCD_Write_DATA.c    -Wextra
	
${OBJECTDIR}/SetupHardware.o: SetupHardware.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SetupHardware.o.d 
	@${RM} ${OBJECTDIR}/SetupHardware.o 
	@${FIXDEPS} "${OBJECTDIR}/SetupHardware.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/SetupHardware.o.d" -o ${OBJECTDIR}/SetupHardware.o SetupHardware.c    -Wextra
	
${OBJECTDIR}/TFTclear.o: TFTclear.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTclear.o.d 
	@${RM} ${OBJECTDIR}/TFTclear.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTclear.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTclear.o.d" -o ${OBJECTDIR}/TFTclear.o TFTclear.c    -Wextra
	
${OBJECTDIR}/TFTcolor.o: TFTcolor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTcolor.o.d 
	@${RM} ${OBJECTDIR}/TFTcolor.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTcolor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTcolor.o.d" -o ${OBJECTDIR}/TFTcolor.o TFTcolor.c    -Wextra
	
${OBJECTDIR}/TFTline.o: TFTline.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTline.o.d 
	@${RM} ${OBJECTDIR}/TFTline.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTline.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTline.o.d" -o ${OBJECTDIR}/TFTline.o TFTline.c    -Wextra
	
${OBJECTDIR}/TFTroundRect.o: TFTroundRect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTroundRect.o.d 
	@${RM} ${OBJECTDIR}/TFTroundRect.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTroundRect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTroundRect.o.d" -o ${OBJECTDIR}/TFTroundRect.o TFTroundRect.c    -Wextra
	
${OBJECTDIR}/bitsets.o: bitsets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/bitsets.o.d 
	@${RM} ${OBJECTDIR}/bitsets.o 
	@${FIXDEPS} "${OBJECTDIR}/bitsets.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/bitsets.o.d" -o ${OBJECTDIR}/bitsets.o bitsets.c    -Wextra
	
${OBJECTDIR}/delay.o: delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/delay.o.d 
	@${RM} ${OBJECTDIR}/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/delay.o.d" -o ${OBJECTDIR}/delay.o delay.c    -Wextra
	
${OBJECTDIR}/sendToS17.o: sendToS17.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sendToS17.o.d 
	@${RM} ${OBJECTDIR}/sendToS17.o 
	@${FIXDEPS} "${OBJECTDIR}/sendToS17.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/sendToS17.o.d" -o ${OBJECTDIR}/sendToS17.o sendToS17.c    -Wextra
	
${OBJECTDIR}/waitAwhile.o: waitAwhile.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/waitAwhile.o.d 
	@${RM} ${OBJECTDIR}/waitAwhile.o 
	@${FIXDEPS} "${OBJECTDIR}/waitAwhile.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/waitAwhile.o.d" -o ${OBJECTDIR}/waitAwhile.o waitAwhile.c    -Wextra
	
${OBJECTDIR}/TFTchar.o: TFTchar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTchar.o.d 
	@${RM} ${OBJECTDIR}/TFTchar.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTchar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTchar.o.d" -o ${OBJECTDIR}/TFTchar.o TFTchar.c    -Wextra
	
${OBJECTDIR}/TFTinit.o: TFTinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTinit.o.d 
	@${RM} ${OBJECTDIR}/TFTinit.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTinit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTinit.o.d" -o ${OBJECTDIR}/TFTinit.o TFTinit.c    -Wextra
	
${OBJECTDIR}/setXY.o: setXY.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/setXY.o.d 
	@${RM} ${OBJECTDIR}/setXY.o 
	@${FIXDEPS} "${OBJECTDIR}/setXY.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/setXY.o.d" -o ${OBJECTDIR}/setXY.o setXY.c    -Wextra
	
${OBJECTDIR}/TFTeraseRect.o: TFTeraseRect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTeraseRect.o.d 
	@${RM} ${OBJECTDIR}/TFTeraseRect.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTeraseRect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTeraseRect.o.d" -o ${OBJECTDIR}/TFTeraseRect.o TFTeraseRect.c    -Wextra
	
${OBJECTDIR}/TFTpixel.o: TFTpixel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTpixel.o.d 
	@${RM} ${OBJECTDIR}/TFTpixel.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTpixel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTpixel.o.d" -o ${OBJECTDIR}/TFTpixel.o TFTpixel.c    -Wextra
	
${OBJECTDIR}/TFTcircle.o: TFTcircle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTcircle.o.d 
	@${RM} ${OBJECTDIR}/TFTcircle.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTcircle.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTcircle.o.d" -o ${OBJECTDIR}/TFTcircle.o TFTcircle.c    -Wextra
	
${OBJECTDIR}/TFTrect.o: TFTrect.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTrect.o.d 
	@${RM} ${OBJECTDIR}/TFTrect.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTrect.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTrect.o.d" -o ${OBJECTDIR}/TFTrect.o TFTrect.c    -Wextra
	
${OBJECTDIR}/TFTprintDec.o: TFTprintDec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTprintDec.o.d 
	@${RM} ${OBJECTDIR}/TFTprintDec.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTprintDec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTprintDec.o.d" -o ${OBJECTDIR}/TFTprintDec.o TFTprintDec.c    -Wextra
	
${OBJECTDIR}/DJS.o: DJS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DJS.o.d 
	@${RM} ${OBJECTDIR}/DJS.o 
	@${FIXDEPS} "${OBJECTDIR}/DJS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DJS.o.d" -o ${OBJECTDIR}/DJS.o DJS.c    -Wextra
	
${OBJECTDIR}/DJSB.o: DJSB.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DJSB.o.d 
	@${RM} ${OBJECTDIR}/DJSB.o 
	@${FIXDEPS} "${OBJECTDIR}/DJSB.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/DJSB.o.d" -o ${OBJECTDIR}/DJSB.o DJSB.c    -Wextra
	
${OBJECTDIR}/TFTprintInt.o: TFTprintInt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTprintInt.o.d 
	@${RM} ${OBJECTDIR}/TFTprintInt.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTprintInt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTprintInt.o.d" -o ${OBJECTDIR}/TFTprintInt.o TFTprintInt.c    -Wextra
	
${OBJECTDIR}/TFTtransitions.o: TFTtransitions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTtransitions.o.d 
	@${RM} ${OBJECTDIR}/TFTtransitions.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTtransitions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTtransitions.o.d" -o ${OBJECTDIR}/TFTtransitions.o TFTtransitions.c    -Wextra
	
${OBJECTDIR}/TFTputchTT.o: TFTputchTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTputchTT.o.d 
	@${RM} ${OBJECTDIR}/TFTputchTT.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTputchTT.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTputchTT.o.d" -o ${OBJECTDIR}/TFTputchTT.o TFTputchTT.c    -Wextra
	
${OBJECTDIR}/TFTputsTT.o: TFTputsTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTputsTT.o.d 
	@${RM} ${OBJECTDIR}/TFTputsTT.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTputsTT.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTputsTT.o.d" -o ${OBJECTDIR}/TFTputsTT.o TFTputsTT.c    -Wextra
	
${OBJECTDIR}/TFTGaddPoint.o: TFTGaddPoint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o.d 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGaddPoint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGaddPoint.o.d" -o ${OBJECTDIR}/TFTGaddPoint.o TFTGaddPoint.c    -Wextra
	
${OBJECTDIR}/TFTGannotate.o: TFTGannotate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGannotate.o.d 
	@${RM} ${OBJECTDIR}/TFTGannotate.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGannotate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGannotate.o.d" -o ${OBJECTDIR}/TFTGannotate.o TFTGannotate.c    -Wextra
	
${OBJECTDIR}/TFTGcolors.o: TFTGcolors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGcolors.o.d 
	@${RM} ${OBJECTDIR}/TFTGcolors.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGcolors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGcolors.o.d" -o ${OBJECTDIR}/TFTGcolors.o TFTGcolors.c    -Wextra
	
${OBJECTDIR}/TFTGexpose.o: TFTGexpose.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGexpose.o.d 
	@${RM} ${OBJECTDIR}/TFTGexpose.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGexpose.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGexpose.o.d" -o ${OBJECTDIR}/TFTGexpose.o TFTGexpose.c    -Wextra
	
${OBJECTDIR}/TFTGinit.o: TFTGinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGinit.o.d 
	@${RM} ${OBJECTDIR}/TFTGinit.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGinit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGinit.o.d" -o ${OBJECTDIR}/TFTGinit.o TFTGinit.c    -Wextra
	
${OBJECTDIR}/TFTGlabels.o: TFTGlabels.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGlabels.o.d 
	@${RM} ${OBJECTDIR}/TFTGlabels.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGlabels.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGlabels.o.d" -o ${OBJECTDIR}/TFTGlabels.o TFTGlabels.c    -Wextra
	
${OBJECTDIR}/TFTGnoCursor.o: TFTGnoCursor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGnoCursor.o.d 
	@${RM} ${OBJECTDIR}/TFTGnoCursor.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGnoCursor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGnoCursor.o.d" -o ${OBJECTDIR}/TFTGnoCursor.o TFTGnoCursor.c    -Wextra
	
${OBJECTDIR}/TFTGnoErase.o: TFTGnoErase.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGnoErase.o.d 
	@${RM} ${OBJECTDIR}/TFTGnoErase.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGnoErase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGnoErase.o.d" -o ${OBJECTDIR}/TFTGnoErase.o TFTGnoErase.c    -Wextra
	
${OBJECTDIR}/TFTGrange.o: TFTGrange.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGrange.o.d 
	@${RM} ${OBJECTDIR}/TFTGrange.o 
	@${FIXDEPS} "${OBJECTDIR}/TFTGrange.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGrange.o.d" -o ${OBJECTDIR}/TFTGrange.o TFTGrange.c    -Wextra
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/TFT.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_AR} $(MP_EXTRA_AR_PRE) r dist/${CND_CONF}/${IMAGE_TYPE}/TFT.X.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    
else
dist/${CND_CONF}/${IMAGE_TYPE}/TFT.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_AR} $(MP_EXTRA_AR_PRE) r dist/${CND_CONF}/${IMAGE_TYPE}/TFT.X.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
