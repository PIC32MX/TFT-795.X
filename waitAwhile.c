/*! \file  waitAwhile.c
 *
 *  \brief Delay using a loop
 *
 *
 *  \author jjmcd
 *  \date December 14, 2013, 9:51 AM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */



/*! waitAwhile - Delay for a selected amount of time */
/*! Delay depending on loop
 *
 */
void waitAwhile( long loopCounter )
{
    long i;

    for ( i=0; i<loopCounter; i++ )
        ;

}
